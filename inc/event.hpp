#ifndef EVENT_H_
#define EVENT_H_

#include<stdint.h>
#include<vector>

#include <pthread.h>

typedef struct _event {
   uint64_t id;
   uint32_t timestamp;
}Event;

// need to call init and uninit before and after filtering
class EventFilter{
    public:
        static pthread_mutex_t _lock;
        static void init(){pthread_mutex_init(&_lock, NULL);};
        static void uninit(){pthread_mutex_destroy(&_lock);};
        static std::vector<Event>& add(std::vector<Event>& events, Event& e);
        static void display(std::vector<Event>& events);
};

//A class that doubles the id
class EventTransformer: public EventFilter{
    public:
        static std::vector<Event>& transform(std::vector<Event>& events);
};

//A class that remove the event whose id is less than the given id
class EventRemove: public EventFilter{
    public:
        static std::vector<Event>& remove(uint64_t id,std::vector<Event>& events);
};

#endif