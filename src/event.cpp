#include<vector>
#include<iostream>
#include<pthread.h>

#include "../inc/event.hpp"

pthread_mutex_t EventFilter::_lock;

std::vector<Event>& EventFilter::add(std::vector<Event>& events, Event& e){
    pthread_mutex_lock(&_lock);
    events.push_back(e);
    pthread_mutex_unlock(&_lock);

    return events;
}

void EventFilter::display(std::vector<Event>& events){
    std::vector<Event>::iterator it;
    
    pthread_mutex_lock(&_lock);

    for(it=events.begin();it!=events.end();it++)
    {
        std::cout<<"id "<<(*it).id<<", timestamp "<<(*it).timestamp<<std::endl;
    }

    pthread_mutex_unlock(&_lock);
}

std::vector<Event>& EventTransformer::transform(std::vector<Event>& events){
    std::vector<Event>::iterator it;

    pthread_mutex_lock(&_lock);

    for(it=events.begin();it!=events.end();it++)
    {
       (*it).id *= 2;
    }

    pthread_mutex_unlock(&_lock);

    return events;
}

std::vector<Event>& EventRemove::remove(uint64_t id, std::vector<Event>& events){
    std::vector<Event>::iterator it;
    pthread_mutex_lock(&_lock);
    for(it=events.begin();it!=events.end();)
    {
       if((*it).id < id){
           it = events.erase(it);
       }else{
           it++;
       }
    }
    pthread_mutex_unlock(&_lock);

    return events;
}