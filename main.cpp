#include<stdint.h>
#include<vector>
#include<iostream>

#include "inc/event.hpp"

int main(void){
    std::vector<Event> events;
    Event e;
    for(int i=0;i<10;i++){
        e.timestamp = i;
        e.id = i;
        events.push_back(e);
    }

    EventFilter::init();

    std::cout<<"original events: "<<std::endl;

    EventFilter::display(events);

    std::cout<<"After filtering: "<<std::endl;

    Event a = {100,100};

    events = EventFilter::add(EventRemove::remove(10, EventTransformer::transform(events)), a);

    EventFilter::display(events);

    EventFilter::uninit();
}